Building Gferasure  
===================

Prequisites
-------------

Before building Gferasure we will need to check what CPU instructions are
currently supported by your machine.  To check what capabilities your machine
has we will need to check the CPU Flags.

Linux: ``cat /proc/cpuinfo | grep -i flag``

Raspbian: ``cat /proc/cpuinfo | grep -i feature``

FreeBSD: ``dmesg -a \| grep -i feature ``

Mac OSX:``machdep.cpu.features && sysctl machdep.cpu.leaf7_features``

CPU Flags:
: **x86: avx2 ssse3 pclmulqdq rdrand**
: **aarch32: neon**
: **aarch64: asimd pmull**


Makefile
-------------

By default the Makefile is setup for machines with avx2, ssse3, clmul, and
rdrand CPU instructions. 

> 13 SUPPORT = avx2 ssse3 clmul rdrand

If your machine does not support avx2 instructions, for example the output
from my Mac laptop shows:

machdep.cpu.features: FPU VME DE PSE TSC MSR PAE MCE CX8 APIC SEP MTRR PGE MCA
CMOV PAT PSE36 CLFSH DS ACPI MMX FXSR SSE SSE2 SS HTT TM PBE SSE3
**PCLMULQDQ** DTES64 MON DSCPL VMX SMX EST TM2 **SSSE3** CX16 TPR
PDCM SSE4.1 SSE4.2 x2APIC POPCNT AES PCID XSAVE OSXSAVE TSCTMR AVX1.0

The above output indicates that this machine does not support AVX2 or RDRAND,
but does support SSSE3 and PCLMULQDQ and therefore we will need to set the
**SUPPORT** value in the Makefile to **ssse3 clmul**.

If none of the supported CPU flags show up from **cat**/**sysctl**, we will 
need to set the value of **SUPPORT** on line 13 to *none* or *leave blank*. The
Makefile will then build the generic, non-simd build for Log/AntiLog and 
ShiftAdd.

> 6 # Set this to one of generic, ssse3, avx2, neon
> 7 SUPPORT = ssse3 clmul

Once the Makefile's **SUPPORT** value has been set to the correct value run
make (or gmake on FreeBSD). Running **make** will build the executables
**gf_test** and **gf_unittest** as well as the shared object library
**gferasure.so** (this is the default name.  If you wish to change it you can
modify **LIBNAME** as long as the name chosen has the suffix .so).

> 15 # This is the name of the library that's produced
> 16 LIBPREF = lib
> 17 LIBNAME = `gferasure.so`

The linker is given an absolute path value to the shared object library, so if
the library is moved, you will need to modify your **LD_LIBRARY_PATH**
environment variable and export it: 
**export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/lib** or alternatively,
re-compile the code using **gcc's** **-L** and **-l** option to tell the
linker which directory to look for the library and what the library's name is.

The **rdrand** flag is not a SIMD instruction, however on newer intel
processors it allows hardware support for generating random numbers.  If
**rdrand** does not show up such as if you have an Apple Macbook Pro prior to
Mid 2011, you will need to remove the **rdrand** flag from the makefile or be
poised with a crypted error: "Illegal Instruction: 4"

Installation by OS / Architecture:
----------------------------------

On each architecture begin with git cloning the repo:

1. git clone https://user@bitbucket.org/ssrc/gferasure/gferasure.git
2. cd gferasure/src
3. Check for the cpuflags, indicated below with double asterix.
4. Set SUPPORT variable based on available CPU flags

(Linux/AVX2)
------------
: `cat /proc/cpuinfo | grep -i flags`
flags: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat
pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp
lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc
aperfmperf eagerfpu pni **pclmulqdq** dtes64 monitor ds_cpl vmx smx est tm2
**ssse3** fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt 
tsc_deadline_timer aes xsave avx f16c **rdrand** lahf_lm abm 3dnowprefetch ida
arat epb pln pts dtherm hwp hwp_noitfy hwp_act_window hwp_epp intel_pt 
tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle **avx2**
smep bmi2 erms invpcid rtm mpx rdseed adx smap clflushopt xsaveopt xsavec 
xgetbv1
> SUPPORT = avx2 ssse3 clmul rdrand

OSX/SSSE3
------------
: `sysctl machdep.cpu.features && sysctl machdep.cpu.leaf7_features`
machdep.cpu.features: FPU VME DE PSE TSC MSR PAE MCE CX8 APIC SEP MTRR PGE
MCA CMOV PAT PSE36 CLFSH DS ACPI MMX FXSR SSE SSE2 SS HTT TM PBE SSE3
**PCLMULQDQ** DTES64 MON DSCPL VMX SMX EST TM2 **SSSE3** CX16 TPR PDCM SSE4.1
SSE4.2 x2APIC POPCNT AES PCID XSAVE OSXSAVE TSCTMR AVX1.0
machdep.cpu.leaf7_features:
> SUPPORT = ssse3 clmul

FreeBSD/SSSE3
------------
: `dmesg -a | grep features`
Features=0x783fbff<FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,
CMOV,PAT,PSE36,MMX,FXSR,SSE,SSE2>
Features2=0x1e98220b<SSE3,MON,SSSE3,CX16,SSE4.1,SSE4.2,POPCNT,AESNI,XSAVE,
OSXSAVE,AVX>
> SUPPORT = ssse3

Raspbian/aarch32
------------
// !! requires installing g++\-5, there is a bug in default g++4.9 for neon !!
: `cat /proc/cpuinfo | grep -i features`
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt
vfpd32 lpae evtstrm crc32
> SUPPORT = neon

Arch Linux/Aarch64
------------
: TBD

After editing the SUPPORT, CXX, and any other variables in the makefile, build
by running ``make`` (on FreeBSD run ``gmake``)

Verify the configuraiton by running unittests for the set of instructions you
are building.

``gf_unittest --erasure=4,4 --coding=csv --unittests=i1dCRPe [fields]``

avx2 fields: gf8_avx2 gf16_avx2 gf8_gather gf16_gather
ssse3 fields: gf8_ssse3 gf16_ssse3
clmul fields: gf8_clmul gf16_clmul gf32_clmul gf64_clmul
generic fields: gf8_shiftadd gf16_shiftadd gf32_shiftadd gf64_shiftadd 
gf8_log gf16_log
neon fields: gf8_vmull gf16_vmull gf32_vmull gf64_vmull gf8_neon gf16_neon


For build or installation issues please create a ticket: 
https://bitbucket.org/ssrc/gferasure/issues

Certain builds are run hourly, the status of the build can be found at:
http://galois.soe.ucsc.edu
