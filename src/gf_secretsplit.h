
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
// gf_secretsplit.h
//
// This include file is used to do secret splitting operations.
// It's designed to be generic, so it could be extended to fields larger than 16 bits (or
// back down to 8 bits if you wanted).
//
// The useful routines are:
//
// secret_split (len, n_shares, n_min):
//      Create a secret split structure that can handle secrets of length len bytes, where
//      n_min are required for recovery and n_shares are generated.
//
// create_shares (secret, share_ids):
//      Create shares from a secret (required).  Optionally, the user can pass in an array
//      of share IDs (unsigneds), with a 0 in the array terminating the list early.  If this
//      array isn't provided, the call will generate its own share IDs.  Share IDs must be
//      no larger than the size of the underlying field (we use 16 bits), and must be non-zero.
//
// get_all_valid_shares (shares):
//      Copies all valid shares into the array of shares passed.  Each share *must* have a
//      valid data pointer, since the data is copied into each share (not passed by reference).
//
// get_all_valid_ids (ids):
//      Copies all valid IDs into the array passed.  Returns the number of valid IDs (shares).
//      Note that if the array pointer is NULL, it just returns the number of valid shares,
//      which might be useful.
//
#pragma once

#include "gf_matrix.h"

struct secret_share {

    secret_share () : id (0U), data ((uint8_t *)0) {}
    unsigned id;
    uint8_t *data;

    secret_share &operator= (const secret_share &a) {
        id = a.id;
        data = a.data;
        return *this;
    }
};

typedef secret_share secret_share_t;

template < typename gf_w, typename gf_state > struct split_secret {

    split_secret (unsigned len, unsigned n_shares, unsigned n_min);
    ~split_secret ();
    int create_shares (void *secret, unsigned *share_ids = (unsigned *)0);
    bool set_share (const secret_share_t *share);
    bool get_share (secret_share_t *share_p);
    int delete_share (unsigned id);
    int find_secret ();
    unsigned get_all_valid_ids (unsigned *ids = (unsigned *)0) const;
    unsigned get_all_valid_shares (secret_share_t *shares) const;
    unsigned find_by_id (unsigned id) const;
    void reset ();

    const void *secret () const { return ((void *)orig[0]); }

    bool is_valid_share (unsigned id) const { return (find_by_id (id) < n_valid_shares); }

    int sanity_check (unsigned n_trials = 1000, unsigned n_random_ids = 0);

    unsigned len;
    unsigned secret_len;
    unsigned n_shares;
    unsigned n_min;
    unsigned n_valid_shares;
    gf_w **orig;
    bool secret_valid;
    uint64_t *region;
    secret_share_t *shares;

    gf_matrix< gf_w, gf_state > generator;
    gf_matrix< gf_w, gf_state > partial;
    gf_matrix< gf_w, gf_state > inv;
    gf_matrix< gf_w, gf_state > single_row;
};
