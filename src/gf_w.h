
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
// only compile file once
#pragma once

#include "gf16_ssse3.h"
#include "gf32_ssse3.h"
#include "gf8_ssse3.h"

#include "gf32_avx_shift2.h"

#include "gf16_clmul.h"
#include "gf32_clmul.h"
#include "gf64_clmul.h"
#include "gf8_clmul.h"

#include "gf16_avx2.h"
#include "gf16_gather.h"
#include "gf8_avx2.h"
#include "gf8_gather.h"

#include "gf16_neon.h"
#include "gf8_neon.h"

#include "gf16_vmull.h"
#include "gf32_vmull.h"
#include "gf64_vmull.h"
#include "gf8_vmull.h"

#include "gf16_pmull.h"
#include "gf32_pmull.h"
#include "gf64_pmull.h"
#include "gf8_pmull.h"

#include "gfw_log.h"
#include "gfw_shift2.h"
#include "gfw_shiftadd.h"

#include "gf_random.h"


// This is the generic template for handling GF operations
// depending on the gf_state passed in, operations will be
// used based on the struct (8,16, or sse based currently)
template < class gf_w, class gf_state > struct gf_w_state {

    gf_w_state (gf_w prim_poly, gf_w alpha = (gf_w)1) : state (prim_poly, alpha) {}

    // + and - are XOR operations
    gf_w add (gf_w a, gf_w b) const { return ((gf_w) (a ^ b)); }
    gf_w sub (gf_w a, gf_w b) const { return ((gf_w) (a ^ b)); }

    // * and / depending on the gf_state
    gf_w mul (gf_w a, gf_w b) const { return state.mul (a, b); }
    gf_w div (gf_w a, gf_w b) const { return state.div (a, b); }
    gf_w inv (gf_w a) const { return state.inv (a); }
    gf_w pow (gf_w a, uint32_t b) const { return state.pow (a, b); }
    gf_w log (gf_w a) const { return state.log (a); }
    gf_w antilog (gf_w a) const { return state.antilog (a); }

    // mul_region: multiply region src by multiplier, and place the result in dst (XOR optional)
    void mul_region (const gf_w *src,
                     gf_w *dst,
                     uint64_t bytes,
                     gf_w multiplier,
                     bool accum = false,
                     void *mult_table = NULL,
                     bool mult_table_valid = false) const {
        state.mul_region (src, dst, bytes, multiplier, accum, mult_table, mult_table_valid);
    }

    // mul_region_region: multiply each element in src1 by the corresponding element in src2,
    // and place the result in dst (XOR optional)
    void mul_region_region (const gf_w *src1,
                            const gf_w *src2,
                            gf_w *dst,
                            uint64_t bytes,
                            bool accum = false) const {
        state.mul_region_region (src1, src2, dst, bytes, accum);
    }

    // Compute the dot product of the two input buffers.  The stride parameter determines the
    // distance between successive used elements.  Strides of 1 and 2 are fastest; other
    // strides might take longer to load from memory.
    gf_w dotproduct (const gf_w *src1,
                     const gf_w *src2,
                     uint64_t n_elements,
                     uint64_t stride = 1) const {
        return state.dotproduct (src1, src2, n_elements, stride);
    }

    uint64_t field_max () const { return state.field_max (); }
    unsigned mult_table_size () const;

    static const unsigned TEST_INV = 0x1;
    static const unsigned TEST_MUL_BY_1 = 0x2;
    static const unsigned TEST_MUL_DIV = 0x4;
    static const unsigned TEST_MUL_REGION_CONST = 0x8;
    static const unsigned TEST_MUL_REGION_REGION = 0x10;
    static const unsigned TEST_DOT_PRODUCT = 0x20;
    static const unsigned TEST_POW = 0x40;
    unsigned unit_test (const char *which_tests, uint64_t test_size, uint64_t bufsize);
    double
    time_trial (gf_w *buf1, gf_w *buf2, gf_w *buf3, uint64_t bytes, char wt, unsigned trials);

    gf_state state;
};
