
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
// gfw_log.cpp
//
// This only implements the (non-folded) log table, for now.

#include "gfw_log.h"
#include <string.h>

template < class gf_w_t > gf_w_t gfw_log_state< gf_w_t >::mul (gf_w_t a, gf_w_t b) const {
    return ((a == 0 || b == 0) ? 0 : antilog_tbl[(uint32_t)log (a) + (uint32_t)log (b)]);
}

/*
 * Raise a to the power of b, within the galois field because
 * of the closed operation, the result will be another element
 * in the field, which we can use the log and anti-log tables
 * to lookup
 */

template < class gf_w_t > gf_w_t gfw_log_state< gf_w_t >::pow (gf_w_t a, uint64_t b) const {
    if (a == 0) {
        return (a);
    } else if (b == 0) {
        return (gf_w_t)1;
    } else {
        return (
        antilog ((gf_w_t) (((uint64_t)log (a) * ((uint64_t)b & field_max ())) % field_max ())));
    }
}

/*
 * Inverse function in a galois field is as simple as taking
 * the field size - 1 for index, and subtracting the value of
 * our log(a), giving us an index into the anti log table
 * which corresponds to the inverse of the log.
 */

template < class gf_w_t > gf_w_t gfw_log_state< gf_w_t >::inv (gf_w_t a) const {
    return ((a > 1) ? antilog_tbl[field_max () - log_tbl[a]] : a);
}

/*
 * Multiplication by region is for multiplying a single value by
 * a region of values, an implementation for multiply one number by
 * many numbers very quickly.
 */

template < class gf_w_t >
void gfw_log_state< gf_w_t >::mul_region (const gf_w_t *src,
                                          gf_w_t *dst,
                                          uint64_t bytes,
                                          gf_w_t multiplier,
                                          bool accum,
                                          void *mult_table,
                                          bool mult_table_valid) const {

    gf_w_t v;

    // Don't complain about these being unused!
    (void)mult_table;
    (void)mult_table_valid;

    if (multiplier == (gf_w_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (multiplier == (gf_w_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t len = bytes;
            if (bitwidth () == (uint64_t)16) len = len >> 1;
            for (uint64_t i = 0; i < len; i++) {
                dst[i] ^= src[i];
            }
        }
        return;
    }


    // conver the multiplier to a unsinged 32 bit value
    uint32_t log_m = (uint32_t) (log (multiplier));

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            // if it is not 0, apply log addition and take anti-log
            // to get back correct multiplication
            v = (src[i] == (gf_w_t)0) ? (gf_w_t)0 : antilog_tbl[log_m + (uint32_t) (log (src[i]))];
            // because this is galois, XOR is add, so add
            // current value v and previously stored value
            dst[i] ^= v;
        }
    } else {
        // do the same as above, but dont XOR/add in value
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            v = (src[i] == (gf_w_t)0) ? (gf_w_t)0 : antilog_tbl[log_m + (uint32_t)log (src[i])];
            dst[i] = v;
        }
    }
}

template < class gf_w_t > unsigned gfw_log_state< gf_w_t >::mult_table_size () const { return 0; }


/*
 * Multiply Region by Region is a multiplication function for multiply two
 * regions of memmory by each other.
*/
template < class gf_w_t >
void gfw_log_state< gf_w_t >::mul_region_region (const gf_w_t *src1,
                                                 const gf_w_t *src2,
                                                 gf_w_t *dst,
                                                 uint64_t bytes,
                                                 bool accum) const {

    gf_w_t v1, v2;

    if (accum) {
        for (uint64_t i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            v1 = src1[i];
            v2 = src2[i];
            dst[i] = (v1 == (gf_w_t)0 || v2 == (gf_w_t)0) ?
                     dst[i] :
                     dst[i] ^ antilog_tbl[(uint32_t)log (v1) + (uint32_t)log (v2)];
        }
    } else {
        for (uint64_t i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            v1 = src1[i];
            v2 = src2[i];
            dst[i] = (v1 == (gf_w_t)0 || v2 == (gf_w_t)0) ?
                     (gf_w_t)0 :
                     antilog_tbl[(uint32_t)log (v1) + (uint32_t)log (v2)];
        }
    }
}

/*
 * Dotproduct is another multiplication function responsible for taking two
 * regions of memmory and multiplying elements from two regions twogether
 * and returning the sum of the products (dot product)
*/

template < class gf_w_t >
gf_w_t gfw_log_state< gf_w_t >::dotproduct (const gf_w_t *src1,
                                            const gf_w_t *src2,
                                            uint64_t n_elements,
                                            uint64_t stride) const {

    gf_w_t dp = (gf_w_t)0;

    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}

template < class gf_w_t > void gfw_log_state< gf_w_t >::setup () {
    assert (sizeof (gf_w_t) <= sizeof (gf_16_t));
    uint32_t b = this->alpha;
    uint32_t i = 1;
    uint32_t reducer = 1U << bitwidth ();
    uint32_t pp = prim_poly ^ reducer;
    // remove the first iteration so compiler doesnt compile about i+field_max
    log_tbl[b] = 0;
    antilog_tbl[0] = (gf_w_t)b;
    antilog_tbl[field_max ()] = (gf_w_t)b;
    b <<= 1;
    for (; i < field_max (); ++i) {
        log_tbl[b] = (gf_w_t)i;
        antilog_tbl[i] = (gf_w_t)b;
        antilog_tbl[i + field_max ()] = (gf_w_t)b;
        b <<= 1;
        if (b & reducer) {
            b ^= pp;
        }
    }
}

template struct gfw_log_state< gf_8_t >;
template struct gfw_log_state< gf_16_t >;

gf8_log_state gf8_log_field (gf8_default_primpoly, (gf_8_t)1);
gf16_log_state gf16_log_field (gf16_default_primpoly, (gf_8_t)1);
