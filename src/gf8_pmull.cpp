
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gf8_pmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include "gf8_pmull.h"

#include <arm_neon.h>
#include <stdio.h>
#include <string.h>

typedef poly16x8_t __v16p;
typedef poly8x8_t __v8x8p;
typedef uint8x8_t __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;

gf_8_t gf8_pmull_state::mul (gf_8_t a8, gf_8_t b8) const {

    __v8x8p a, b, pp;
    __v16p result, w;
    uint8x8_t v;

    a = vcreate_p8 (a8);
    b = vcreate_p8 (b8);
    pp = vreinterpret_p8_p16 (vcreate_p16 (0x11d));

    result = vmull_p8 (a, b);
    v = vshrn_n_u16 (vreinterpretq_u16_p16 (result), 8);
    w = vmull_p8 (pp, vreinterpret_p8_u8 (v));
    v = vshrn_n_u16 (vreinterpretq_u16_p16 (w), 8); // let complier do v and result
    result =
    vreinterpretq_p16_u8 (veorq_u8 (vreinterpretq_u8_p16 (result), vreinterpretq_u8_p16 (w)));
    w = vmull_p8 (pp, vreinterpret_p8_u8 (v));
    result =
    vreinterpretq_p16_u8 (veorq_u8 (vreinterpretq_u8_p16 (result), vreinterpretq_u8_p16 (w)));
    return vgetq_lane_u8 (vreinterpretq_u8_p16 (result), 0);
}

static inline __v8x8u shuf (uint8x8x2_t a, __v8x8u b) { return vtbl2_u8 (a, b); }

static inline uint8x8x2_t to_split (__v8u v) {
    return uint8x8x2_t ({ vget_low_u8 (v), vget_high_u8 (v) });
}

static inline __v8x8u mul_mult (__v8x8u a8, __v8x8p b8) {
    __v8x8p a, b, pp;
    __v16p result, w1, w2;
    __v16u v;
    __v8x8u mask = vcreate_u8 (0x0d0c090805040100);

    pp = vreinterpret_p8_p16 (vdup_n_p16 ((poly16_t)0x11d));
    a = vreinterpret_p8_u8 (a8);
    b = b8;

    result = vmull_p8 (a, b);

    v = vshrq_n_u16 (vreinterpretq_u16_p16 (result), 8);
    w1 = vmull_p8 (pp, vreinterpret_p8_u16 (vget_low_u16 (v)));
    w2 = vmull_p8 (pp, vreinterpret_p8_u16 (vget_high_u16 (v)));
    w1 = vreinterpretq_p16_u8 (vcombine_u8 (shuf (to_split (vreinterpretq_u8_p16 (w1)), mask),
                                            shuf (to_split (vreinterpretq_u8_p16 (w2)), mask)));

    result =
    vreinterpretq_p16_u16 (veorq_u16 (vshlq_n_u16 ((v), 8), vreinterpretq_u16_p16 (result)));
    result =
    vreinterpretq_p16_u8 (veorq_u8 (vreinterpretq_u8_p16 (result), vreinterpretq_u8_p16 (w1)));

    v = vshrq_n_u16 (vreinterpretq_u16_p16 (result), 8);
    w1 = vmull_p8 (pp, vreinterpret_p8_u16 (vget_low_u16 (v)));
    w2 = vmull_p8 (pp, vreinterpret_p8_u16 (vget_high_u16 (v)));
    w1 = vreinterpretq_p16_u8 (vcombine_u8 (shuf (to_split (vreinterpretq_u8_p16 (w1)), mask),
                                            shuf (to_split (vreinterpretq_u8_p16 (w2)), mask)));
    result =
    vreinterpretq_p16_u8 (veorq_u8 (vreinterpretq_u8_p16 (result), vreinterpretq_u8_p16 (w1)));

    mask = vmovn_u16 (vreinterpretq_u16_p16 (result));

    return mask;
}

unsigned gf8_pmull_state::mult_table_size () const { return (0); }

void gf8_pmull_state::mul_region (const gf_8_t *src,
                                  gf_8_t *dst,
                                  uint64_t bytes,
                                  gf_8_t m,
                                  bool accum,
                                  void *mult_table,
                                  bool mult_table_valid) const {
    if (m == (gf_8_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (m == (gf_8_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t chunks = bytes >> 3ULL;
            for (uint64_t i = 0; i < chunks; i += 2) {
                uint64x2_t x = vld1q_u64 ((uint64_t *)src + i);
                uint64x2_t y = vld1q_u64 ((uint64_t *)dst + i);
                vst1q_u64 ((uint64_t *)dst + i, veorq_u64 (x, y));
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    uint64_t i;
    __v8x8p mm = vdup_n_p8 (m);
    uint64_t chunks = bytes >> 3ULL;
    if (accum) {
        for (i = 0; i < chunks; i++) {

            __v8x8u x = vld1_u8 (src + (i * 8));
            __v8x8u y = vld1_u8 (dst + (i * 8));
            vst1_u8 (dst + (i * 8), veor_u8 (y, mul_mult (x, mm)));
        }
    } else {
        for (i = 0; i < chunks; i++) {
            __v8x8u x = vld1_u8 (src + (i * 8));
            vst1_u8 (dst + (i * 8), mul_mult (x, mm));
        }
    }
}

void gf8_pmull_state::mul_region_region (const gf_8_t *src1,
                                         const gf_8_t *src2,
                                         gf_8_t *dst,
                                         uint64_t bytes,
                                         bool accum) const {

    uint64_t i;
    uint64_t chunks = bytes; // >> 3ULL;

    if (accum) {
        for (i = 0; i < chunks; i++) {
            dst[i] = add (dst[i], mul (src1[i], src2[i]));
        }
    } else {
        for (i = 0; i < chunks; i++) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

gf_8_t gf8_pmull_state::dotproduct (const gf_8_t *src1,
                                    const gf_8_t *src2,
                                    uint64_t n_elements,
                                    uint64_t stride) const {

    gf_8_t dp = (gf_8_t)0;

    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}
