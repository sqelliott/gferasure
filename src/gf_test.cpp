
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/

//
// Performance test harness for Gferasure
//

#include <ctype.h>
#include <getopt.h> // included for user input
#include <stdio.h>  // for printf and stdout
#include <stdlib.h> // for strtol and exit
#include <string.h>
#include <string>     // string
#include <sys/time.h> // for the gettimeofday

#include "gf_test.h"

// struct for storing user options
struct option long_options[] = { { "coding", required_argument, 0, 'c' },
                                 { "data", required_argument, 0, 'd' },
                                 { "stripe", required_argument, 0, 'p' },
                                 { "encode", required_argument, 0, 'e' },
                                 { "app", required_argument, 0, 'A' },
                                 { "shares", required_argument, 0, 's' },
                                 { "min_shares", required_argument, 0, 'm' },
                                 { "bytes", required_argument, 0, 'b' },
                                 { "trials", required_argument, 0, 't' },
                                 { "gf", required_argument, 0, 'g' },
                                 { "random_ids", no_argument, 0, 'r' },
                                 { "timed", required_argument, 0, 'T' },
                                 { 0, 0, 0, 0 } };


void print_help (char *prog_name) {
    fprintf (stderr, "Usage: %s[options]\n", prog_name);
    fprintf (stderr, "Example 1: %s -t 50 -b 768231 -Tm -g gf32_shiftadd\n", prog_name);
    fprintf (stderr, "Example 2: %s -As -s 36 -m 8 -r\n", prog_name);
    fprintf (stderr, "\t-t, --trials\t\tNumber of trials to run (default=1000)\n");
    fprintf (stderr, "\t-b, --bytes\t\tSize of secret to split / rebuild (default=1024)\n");
    fprintf (stderr, "\tEncoding specific arguments:\n");
    fprintf (stderr, "\t-c, --coding=<c|s|v>\n");
    fprintf (stderr, "\t\tc: non-systematic Cauchy\n");
    fprintf (stderr, "\t\ts: systematic Cauchy\n");
    fprintf (stderr, "\t\tv: non-systematic Vandermonde\n");
    fprintf (stderr, "\t-d, --data\t\tNumber of data symbols to use (default=6)\n");
    fprintf (stderr, "\t-e, --encode\t\tNumber of coded symbols to use (default=3)\n");
    fprintf (stderr,
             "\t-p, --stripe\t\tSpecify a region size for matrix multiplication (default=none)\n");
    fprintf (stderr, "\tSecret Splitting specific arguments:\n");
    fprintf (stderr, "\t-s, --shares\t\tNumber of shares to split (default=24)\n");
    fprintf (stderr,
             "\t-m, --min_shares\tMinimum number of shares to rebuild (default=2+shares/2\n");
    fprintf (stderr, "\t-r, --random_ids\tUse random share IDs (default is sequential)\n");
    fprintf (stderr, "\t-l, --limit_ids\t\tUse a limited number of IDs\n");
    fprintf (stderr, "\tPerformance Testing arguments\n");
    fprintf (stderr, "\t-T, --timed\t\tTime test for Galois field (m = region/constant, "
                     "r=region/region, d=dot product\n");
    fprintf (stderr,
             "\t-A, --app\t\tTime test an application (s=secret splitting, e=erasure coding\n");
    fprintf (stderr, "\t-g, --gf\t\tSpecify a Galois field implementation (default=gf8_log):\n");
    for (unsigned i = 0; i < n_encode_fields; ++i) {
        fprintf (stderr, "\t\t%s\n", encode_test_fields[i].field_name);
    }
    exit (-99);
}

int main (int argc, char *argv[]) {

    // number of shares for secret splitting
    unsigned n_shares = 24;
    // number of minimum shares to reconstruct
    unsigned n_min = 2 + n_shares / 2;
    // number of bytes for region (secret+encode)
    unsigned n_bytes = 1024;
    // run for x number of trials
    unsigned n_trials = 1000;
    // how many data elements for encoding
    unsigned n_data = 6;
    // number of coded symbols for encoding
    unsigned n_ecc = 3;
    // stripe size if chosen
    unsigned stripe = 0;

    // user can select
    unsigned ecc_mode = ECC_CAUCHY;
    const char *ecc_mode_name = "CAUCHY";

    const char valid_ecc_tests[] = "cvs";
    unsigned n_random_ids = 0;
    int c;
    char timed_test = '\0';
    char app_test = '\0';
    double compute_time = 0.0;
    const char *gf_str = "gf8_log";

    if (argc == 1) {
        print_help (argv[0]);
    }

    // read in user inputs, store in appropriate variables
    while (1) {
        c = getopt_long (argc, argv, "c:d:e:p:s:m:b:t:l:X:T:A:g:rh?", long_options, NULL);

        if (c == -1) {
            break;
        }

        switch (c) {
        case 'c':
            ecc_mode = tolower (optarg[0]);
            if (strchr (valid_ecc_tests, ecc_mode) == NULL) {
                fprintf (stderr, "%s: invalid ecc test %c\n", argv[0], c);
                print_help (argv[0]);
            }
            if (ecc_mode == 'c') {
                ecc_mode = ECC_CAUCHY;
                ecc_mode_name = const_cast< const char * > ("CAUCHY");
                break;
            }
            if (ecc_mode == 'v') {
                ecc_mode = ECC_VANDERMONDE;
                ecc_mode_name = const_cast< const char * > ("VANDERMONDE");
                break;
            }
            if (ecc_mode == 's') {
                ecc_mode = ECC_CAUCHY_SYSTEMATIC;
                ecc_mode_name = const_cast< const char * > ("SYSTEMATIC CAUCHY");
                break;
            }
            break;
        case 'd':
            n_data = strtol (optarg, NULL, 10);
            break;
        case 'e':
            n_ecc = strtol (optarg, NULL, 10);
            break;
        case 'p':
            stripe = strtol (optarg, NULL, 10);
            break;
        case 's':
            n_shares = strtol (optarg, NULL, 10);
            break;
        case 'm':
            n_min = strtol (optarg, NULL, 10);
            break;
        case 'b':
            n_bytes = strtol (optarg, NULL, 10);
            break;
        case 't':
            n_trials = strtol (optarg, NULL, 10);
            break;
        case 'r':
            n_random_ids = n_shares;
            break;
        case 'g':
            gf_str = optarg;
        case 'l':
            n_random_ids = strtol (optarg, NULL, 10);
            break;
        case 'T':
            timed_test = tolower (optarg[0]);
            if (!strchr ("mrd", timed_test)) {
                print_help (argv[0]);
            }
            break;
        case 'A':
            app_test = tolower (optarg[0]);
            if (!strchr ("se", app_test)) {
                print_help (argv[0]);
            }
            break;
        case 'h':
        case '?':
            print_help (argv[0]);
            break;
        default:
            fprintf (stderr, "%s: option %c not recognized!\n", argv[0], c);
            print_help (argv[0]);
            break;
        }
    }

    if (n_trials < 1 || n_trials > 100000) {
        fprintf (stderr, "%s: trials (%d) must be 1-100000\n", argv[0], n_trials);
        print_help (argv[0]);
    }


    int index = -1;
    for (unsigned j = 0; j < n_encode_fields; ++j) {
        if (!strcmp (gf_str, encode_test_fields[j].field_name)) {
            index = j;
        }
    }
    if (index < 0) {
        fprintf (stderr, "%s: Invalid field type!\n", gf_str);
        exit (1);
    }

    if (timed_test != '\0' && app_test != '\0') {
        fprintf (stderr, "Both performance tests set - selecting only -T option\n");
    }
    // if we are not running a timed_test
    if (timed_test == '\0') {
        // and not running an app test
        if (app_test == '\0') {
            fprintf (stderr,
                     "Select either a timed test (-T) or an application test (-A) to run\n");
            exit (-50);
        } else {
            // we are running secret split test
            if (app_test == 's') {
                if (n_shares < 4 || n_shares > 200) {
                    fprintf (stderr, "%s: shares (%d) must be between 4 and 200 (inclusive)\n",
                             argv[0], n_shares);
                    print_help (argv[0]);
                }

                if (n_min == 0) {
                    n_min = (n_shares / 2) + 2;
                } else if (n_min > n_shares || n_min < 2) {
                    fprintf (stderr,
                             "%s: minimum shares (%d) must be between 2 and n_shares (inclusive)\n",
                             argv[0], n_min);
                    print_help (argv[0]);
                }

                if (n_random_ids > n_shares) {
                    fprintf (
                    stderr, "%s: number of random IDs (%d) must be no greater than n_shares (%d)\n",
                    argv[0], n_random_ids, n_shares);
                    print_help (argv[0]);
                }

                if (n_bytes % 16 != 0 || n_bytes > 1024 * 1024 * 32) {
                    fprintf (stderr,
                             "%s: bytes (%d) must be less than 32MB and evenly divisible by 16\n",
                             argv[0], n_bytes);
                    print_help (argv[0]);
                }

                const struct secret_split_info *fld = &(secret_test_fields[index]);
                compute_time = fld->perf (n_shares, n_min, n_bytes, n_trials, n_random_ids);

                printf ("%s: test %c took %.8f seconds for (shares=%d min=%d bytes=%d trials=%d) "
                        "(%.2f MB/s)\n",
                        gf_str, app_test, compute_time, n_shares, n_min, n_bytes, n_trials,
                        ((double)n_trials * (double)n_bytes) / (compute_time * 1024.0 * 1024.0));

            }
            // we are doing encoding
            else if (app_test == 'e') {
                if (n_data == 0) {
                    fprintf (stderr, "%s: number of data symbols must be greater than 0\n",
                             argv[0]);
                    print_help (argv[0]);
                }
                if (n_ecc == 0) {
                    fprintf (stderr, "%s: number of coded symbols must be greater than 0\n",
                             argv[0]);
                    print_help (argv[0]);
                }
                if (stripe > n_bytes) {
                    fprintf (stderr, "%s: stripe size must be less than number of bytes\n",
                             argv[0]);
                    print_help (argv[0]);
                }

                const struct encode_info *fld = &(encode_test_fields[index]);
                compute_time = fld->perf (n_data, n_ecc, n_bytes, n_trials, stripe, ecc_mode);

                printf ("%s: test %c took %.8f seconds for (data=%d ecc=%d code=%s bytes=%d "
                        "trials=%d, stipe=%d) (%.2f MB/s)\n",
                        gf_str, app_test, compute_time, n_data, n_ecc, ecc_mode_name, n_bytes,
                        n_trials, stripe,
                        ((double)n_trials * (double)n_bytes) / (compute_time * 1024.0 * 1024.0));
            }
        }
    } else {
        gf_32_t *buf1 = new gf_32_t[n_bytes / sizeof (gf_32_t)];
        gf_32_t *buf2 = new gf_32_t[n_bytes / sizeof (gf_32_t)];
        gf_32_t *buf3 = new gf_32_t[n_bytes / sizeof (gf_32_t)];
        for (unsigned i = 0; i < n_bytes / sizeof (gf_32_t); ++i) {
            buf1[i] = gf_random_val< gf_32_t > ();
            buf2[i] = gf_random_val< gf_32_t > ();
        }

        const struct galois_info *fld = &(test_fields[index]);
        compute_time = fld->perf (buf1, buf2, buf3, n_bytes, timed_test, n_trials);

        printf ("%s: test %c took %.8f seconds for %d trials of %d bytes (%.2f MB/s)\n", gf_str,
                timed_test, compute_time, n_trials, n_bytes,
                ((double)n_trials * (double)n_bytes) / (compute_time * 1024.0 * 1024.0));
    }
}
