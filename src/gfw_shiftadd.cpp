/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gfw_shiftadd.cpp
 *
 * Routines for n-bit Galois fields using simple shift and add.  These should be
 * independent of field width, assuming that there's a function that returns element
 * size in bits.
 *
 */

#include "gfw_shiftadd.h"
#include <string.h>

#include <stdio.h>

template < class gf_w_t > gf_w_t gfw_shiftadd_state< gf_w_t >::mul (gf_w_t a, gf_w_t b) const {
    gf_w_t accum = (b & (gf_w_t)1) ? a : (gf_w_t)0;

    for (unsigned i = 1; i < bitwidth (); ++i) {
        a = mul_by_2 (a);
        b >>= 1;
        accum = add (accum, (b & (gf_w_t)1) ? a : 0);
    }
    return accum;
}

template < class gf_w_t > gf_w_t gfw_shiftadd_state< gf_w_t >::pow (gf_w_t a, uint64_t b) const {
    gf_w_t accum = (b & 1) ? a : (gf_w_t)1;
    gf_w_t powers = a;

    b >>= 1;
    while (b != 0) {
        powers = mul (powers, powers);
        if (b & 1) {
            accum = mul (accum, powers);
        }
        b >>= 1;
    }
    return accum;
}

template < class gf_w_t > gf_w_t gfw_shiftadd_state< gf_w_t >::inv (gf_w_t a) const {
    return (a < 2) ? a : pow (a, (0xffffffffffffffffULL >> (64ULL - bitwidth ())) ^ 0x1);
}

template < class gf_w_t > unsigned gfw_shiftadd_state< gf_w_t >::mult_table_size () const {
    return (0);
}

template < class gf_w_t >
void gfw_shiftadd_state< gf_w_t >::mul_region (const gf_w_t *src,
                                               gf_w_t *dst,
                                               uint64_t bytes,
                                               gf_w_t multiplier,
                                               bool accum,
                                               void *mult_table,
                                               bool mult_table_valid) const {

    if (multiplier == (gf_w_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (multiplier == (gf_w_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            for (uint64_t i = 0; i < bytes / sizeof (gf_w_t); i++) {
                dst[i] = add (dst[i], src[i]);
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = add (dst[i], mul (multiplier, src[i]));
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = mul (multiplier, src[i]);
        }
    }
}

template < class gf_w_t >
void gfw_shiftadd_state< gf_w_t >::mul_region_region (const gf_w_t *src1,
                                                      const gf_w_t *src2,
                                                      gf_w_t *dst,
                                                      uint64_t bytes,
                                                      bool accum) const {

    if (accum) {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = add (dst[i], mul (src1[i], src2[i]));
        }
    } else {
        for (unsigned i = 0; i < bytes / sizeof (gf_w_t); ++i) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

template < class gf_w_t >
gf_w_t gfw_shiftadd_state< gf_w_t >::dotproduct (const gf_w_t *src1,
                                                 const gf_w_t *src2,
                                                 uint64_t n_elements,
                                                 uint64_t stride) const {
    gf_w_t dp = (gf_w_t)0;

    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}

template struct gfw_shiftadd_state< gf_8_t >;
template struct gfw_shiftadd_state< gf_16_t >;
template struct gfw_shiftadd_state< gf_32_t >;
template struct gfw_shiftadd_state< gf_64_t >;

gf8_shiftadd_state gf8_shiftadd_field (gf8_default_primpoly, (gf_8_t)1);
gf16_shiftadd_state gf16_shiftadd_field (gf16_default_primpoly, (gf_8_t)1);
gf32_shiftadd_state gf32_shiftadd_field (gf32_default_primpoly, (gf_8_t)1);
gf64_shiftadd_state gf64_shiftadd_field (gf64_default_primpoly, (gf_8_t)1);
