
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gf64_clmul.cpp
 *
 * Routines for 64-bit Galois fields using SSSE3.
 *
 */

#include "gf64_clmul.h"

#include <string.h>
#include <x86intrin.h>

inline static __m128i mul_2 (__m128i a, __m128i b, __m128i pp) {
    __m128i res0, res1;
    __m128i z0 = _mm_set_epi32 (0xffffffff, 0, 0xffffffff, 0xffffffff);
    __m128i z1 = _mm_set_epi32 (0, 0xffffffff, 0xffffffff, 0xffffffff);
    __m128i v0, w0;

    res0 = _mm_clmulepi64_si128 (a, b, 0);
    v0 = _mm_and_si128 (res0, z0);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0x10);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_and_si128 (res0, z1);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0x10);
    res0 = _mm_xor_si128 (res0, w0);

    res1 = _mm_clmulepi64_si128 (a, b, 0x11);
    v0 = _mm_and_si128 (res1, z0);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0x10);
    res1 = _mm_xor_si128 (res1, w0);
    v0 = _mm_and_si128 (res1, z1);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0x10);
    res1 = _mm_xor_si128 (res1, w0);

    return _mm_xor_si128 (_mm_move_epi64 (res0), _mm_slli_si128 (res1, 8));
}


gf_64_t gf64_clmul_state::mul (gf_64_t a64, gf_64_t b64) const {
    __m128i a, b;
    __m128i result;
    __m128i pp;
    __m128i v0, w0;
    __m128i z0 = _mm_set_epi32 (0xffffffff, 0, 0xffffffff, 0xffffffff);
    __m128i z1 = _mm_set_epi32 (0, 0xffffffff, 0xffffffff, 0xffffffff);

    a = _mm_insert_epi64 (_mm_setzero_si128 (), a64, 0);
    b = _mm_insert_epi64 (a, b64, 0);
    pp = _mm_set_epi32 (0, 0, 0, (uint32_t)prim_poly);

    result = _mm_clmulepi64_si128 (a, b, 0);
    v0 = _mm_and_si128 (result, z0);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0x10);
    result = _mm_xor_si128 (result, w0);
    v0 = _mm_and_si128 (result, z1);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0x10);
    result = _mm_xor_si128 (result, w0);

    return (gf_64_t)_mm_movepi64_pi64 (result);
}

unsigned gf64_clmul_state::mult_table_size () const { return (0); }

void gf64_clmul_state::mul_region (const gf_64_t *src,
                                   gf_64_t *dst,
                                   uint64_t bytes,
                                   gf_64_t m,
                                   bool accum,
                                   void *mult_table,
                                   bool mult_table_valid) const {
    if (m == (gf_64_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (m == (gf_64_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t chunks = bytes >> 4ULL;
            for (uint64_t i = 0; i < chunks; i++) {
                __m128i x = _mm_loadu_si128 (((__m128i *)src) + i);
                __m128i y = _mm_loadu_si128 (((__m128i *)dst) + i);
                _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128 (x, y));
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    uint64_t i;
    uint64_t chunks = bytes >> 4ULL;
    __m128i pp = _mm_set_epi32 (0, (uint32_t) (prim_poly & 0xffffffffULL), 0,
                                (uint32_t) (prim_poly & 0xffffffffULL));
    __m128i mult = _mm_set1_epi64x (m);

    if (accum) {
        __m128i x, y;
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src) + i);
            y = _mm_loadu_si128 (((__m128i *)dst) + i);
            _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128 (mul_2 (x, mult, pp), y));
        }
    } else {
        __m128i x;
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src) + i);
            _mm_storeu_si128 (((__m128i *)dst) + i, mul_2 (x, mult, pp));
        }
    }
}

void gf64_clmul_state::mul_region_region (const gf_64_t *src1,
                                          const gf_64_t *src2,
                                          gf_64_t *dst,
                                          uint64_t bytes,
                                          bool accum) const {

    uint64_t i;
    uint64_t chunks = bytes >> 4ULL;
    __m128i pp = _mm_set_epi32 (0, (uint32_t) (prim_poly & 0xffffffffULL), 0,
                                (uint32_t) (prim_poly & 0xffffffffULL));

    if (accum) {
        __m128i x, y, z;
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src1) + i);
            y = _mm_loadu_si128 (((__m128i *)src2) + i);
            z = _mm_loadu_si128 (((__m128i *)dst) + i);
            _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128 (mul_2 (x, y, pp), z));
        }
    } else {
        __m128i x, y;
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src1) + i);
            y = _mm_loadu_si128 (((__m128i *)src2) + i);
            _mm_storeu_si128 (((__m128i *)dst) + i, mul_2 (x, y, pp));
        }
    }
}

gf_64_t gf64_clmul_state::dotproduct (const gf_64_t *src1,
                                      const gf_64_t *src2,
                                      uint64_t n_elements,
                                      uint64_t stride) const {

    /*
    if (stride == 1 && n_elements > 1 && !(n_elements&1)) {
      __m64 dp = _m_from_int64(0);
      __m128i x,y;
      __m128i pp = _mm_set_epi32( 0, (uint32_t)(prim_poly & 0xffffffffULL),
                                  0, (uint32_t)(prim_poly & 0xffffffffULL));

      for (uint64_t i = 0; i < n_elements; i+=2) {
        x = _mm_loadu_si128 (((__m128i *)src1) + i);
        y = _mm_loadu_si128 (((__m128i *)src2) + i);
        x = mul_2(x,y,pp);
        y = _mm_srli_si128(x,8);
        x = _mm_xor_si128(x,y);
        dp = _mm_xor_si64(dp,_m_from_int64(_mm_extract_epi64(x,0)));
      }
      return _m_to_int64(dp);
    } else {
    }
    */
    gf_64_t dp = (gf_64_t)0;
    if (stride == 0 && n_elements > 1 && !(n_elements & 1)) {
        __m128i x, y, z;
        __m128i pp = _mm_set_epi32 (0, (uint32_t) (prim_poly & 0xffffffffULL), 0,
                                    (uint32_t) (prim_poly & 0xffffffffULL));

        for (uint64_t i = 0; i < n_elements; i += 2) {
            x = _mm_loadu_si128 (((__m128i *)src1) + i);
            y = _mm_loadu_si128 (((__m128i *)src2) + i);
            z = mul_2 (x, y, pp);
            dp ^= add (_mm_extract_epi64 (z, 0), _mm_extract_epi64 (z, 1));
        }
    } else {
        for (uint64_t i = 0; i < n_elements; ++i) {
            dp = add (dp, mul (*src1, *src2));
            src1 += stride;
            src2 += stride;
        }
    }
    return dp;
}
