/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/

//
// gf32_ssse3_shift2.cpp
//
// Routines for 32-bit Galois fields using 2-bit shift-add under SSSE3.
//


#include <stdio.h>

#include "gf32_avx_shift2.h"
#include <string.h>
#include <x86intrin.h>

void gf32_avx_shift2_state::setup () {}

static __m128i setup_multiplier (gf_32_t m, uint32_t pp) {
    __m128i mult;
    uint32_t m2 = m << 1;

    if (m & 0x80000000) {
        m2 ^= pp;
    }
    // Set up the multiplier with 3, 2, 1 and 0 multiples of m.
    mult = _mm_set_epi32 (m2 ^ m, m2, m, 0);
    return mult;
}

// clang doesnt mind the conversion, gcc does
#if defined(APPLE) || defined(FREEBSD)
static inline __m128i mul_vec_by_const_16 (__m128i v, __m128i multiplier, __m128i reducer) {
    __m128i t;
    __m128i result;

    // Handle the first set of values
    result = _mm_permutevar_ps (multiplier, v);
    // Handle the remainder of the values
    for (unsigned i = 0; i < 15; ++i) {
        // Shift multiplier right by 2 and reduce
        t = _mm_permutevar_ps (reducer, _mm_srli_epi32 (multiplier, 30));
        multiplier = _mm_xor_si128 (t, _mm_slli_epi32 (multiplier, 2));
        // Shift src by 2 to get the next two bits to multiply
        v = _mm_srli_epi32 (v, 2);
        t = _mm_permutevar_ps (multiplier, v);
        result = _mm_xor_si128 (result, t);
    }
    return (result);
}
#else
static inline __m128i mul_vec_by_const_16 (__m128i v, __m128i multiplier, __m128i reducer) {
    __m128 t;
    __m128 result;

    // Handle the first set of values
    result = _mm_permutevar_ps (_mm_castsi128_ps (multiplier), v);
    // Handle the remainder of the values
    for (unsigned i = 0; i < 15; ++i) {
        // Shift multiplier right by 2 and reduce
        t = _mm_permutevar_ps (_mm_castsi128_ps (reducer), _mm_srli_epi32 (multiplier, 30));
        multiplier = _mm_xor_si128 (_mm_castps_si128 (t), _mm_slli_epi32 (multiplier, 2));
        // Shift src by 2 to get the next two bits to multiply
        v = _mm_srli_epi32 (v, 2);
        t = _mm_permutevar_ps (_mm_castsi128_ps (multiplier), v);
        result = _mm_castsi128_ps (_mm_xor_si128 (_mm_castps_si128 (result), _mm_castps_si128 (t)));
    }
    return (_mm_castps_si128 (result));
}
#endif

unsigned gf32_avx_shift2_state::mult_table_size () const { return (0); }

void gf32_avx_shift2_state::mul_region (const gf_32_t *src,
                                        gf_32_t *dst,
                                        uint64_t bytes,
                                        gf_32_t m,
                                        bool accum,
                                        void *mult_table,
                                        bool mult_table_valid) const {
    __m128i multiplier = setup_multiplier (m, prim_poly);
    __m128i reducer;
    __m128i v, result;
    uint64_t n_chunks = bytes / sizeof (__m128i);
    uint64_t i;

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    if (m > (gf_32_t)1) {
        // Assuming that the leading bit (bit 31) of the primitive polynomial is a 0, the
        // reducer now contains values that properly reduce for bits 00, 01, 10, and 11.
        // This is used to quickly multiply the multiplier by 4.
        reducer = _mm_set_epi32 ((prim_poly << 1U) ^ prim_poly, (prim_poly << 1U), prim_poly, 0);

        if (accum) {
            for (i = 0; i < n_chunks; ++i) {
                v = _mm_loadu_si128 ((__m128i *)src + i);
                result = _mm_xor_si128 (mul_vec_by_const_16 (v, multiplier, reducer),
                                        _mm_loadu_si128 ((__m128i *)dst + i));
                _mm_storeu_si128 ((__m128i *)dst + i, result);
            }
            for (i = n_chunks << 4ULL; i < bytes; ++i) {
                dst[i] = add (mul (src[i], m), dst[i]);
            }
        } else {
            for (i = 0; i < n_chunks; ++i) {
                v = _mm_loadu_si128 ((__m128i *)src + i);
                result = mul_vec_by_const_16 (v, multiplier, reducer);
                _mm_storeu_si128 ((__m128i *)dst + i, result);
            }
            for (i = n_chunks << 4ULL; i < bytes; ++i) {
                dst[i] = mul (src[i], m);
            }
        }
    } else if (m == (gf_32_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
    } else { // m == 1
        if (accum) {
            for (uint64_t i = 0; i < n_chunks; ++i) {
                v = _mm_loadu_si128 ((__m128i *)src + i);
                result = _mm_loadu_si128 ((__m128i *)dst + i);
                _mm_storeu_si128 ((__m128i *)dst + i, _mm_xor_si128 (result, v));
            }
            for (i = n_chunks << 4ULL; i < bytes; ++i) {
                dst[i] ^= src[i];
            }
        } else {
            memcpy (dst, src, bytes);
        }
    }
}

void gf32_avx_shift2_state::mul_region_region (const gf_32_t *src1,
                                               const gf_32_t *src2,
                                               gf_32_t *dst,
                                               uint64_t bytes,
                                               bool accum) const {
    return shiftadd_state.mul_region_region (src1, src2, dst, bytes, accum);
}

gf_32_t gf32_avx_shift2_state::dotproduct (const gf_32_t *src1,
                                           const gf_32_t *src2,
                                           uint64_t n_elements,
                                           uint64_t stride) const {
    return shiftadd_state.dotproduct (src1, src2, n_elements, stride);
}
