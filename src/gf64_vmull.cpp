
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gf64_vmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include <arm_neon.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "gf64_vmull.h"

typedef poly16x8_t __v16p;
typedef poly8x8_t __v8x8p;
typedef uint8x8_t __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;
typedef uint64x2_t __v64u;

/*Camara et al.'s implementation:
Fast Software Polynomial Multiplication on ARM processors Using the NEON Engine
*/
#ifdef GF_ARMV7
static inline __v16u __attribute__ ((optimize ("O0"))) mul128 (uint64x1_t a, uint64x1_t b) {
    // q0 = t0q, d0 = t0l, d1 = t0h
    // q1 = t1q, d2 = t1l, d3 = t1h
    // q2 = t2q, d4 = t2l, d5 = t2h
    // q3 = t3q, d6 = t3l, d6 = t3h
    // q4 = rq,  d8 = rl,  d9 = rh
    // d10 = a
    // d11 = b
    // d12 = k16
    // d13 = k32
    // d14 = k48
    register uint16x8_t rq asm("q4");
    register uint64x1_t ad asm("d10") = a;
    register uint64x1_t bd asm("d11") = b;
    register uint64x1_t k16 asm("d12") = vcreate_u64 (0xffff);
    ;
    register uint64x1_t k32 asm("d13") = vcreate_u64 (0xffffffff);
    register uint64x1_t k48 asm("d14") = vcreate_u64 (0xffffffffffff);
    asm("vext.8 d0, d10, d10, $1\n\t"                    // 1
        "vmull.p8 q0, d0, d11\n\t"                       // 2
        "vext.8 d8, d11, d11, $1\n\t"                    // 3
        "vmull.p8 q4, d10, d8\n\t"                       // 4
        "vext.8 d2, d10, d10, $2\n\t"                    // 5
        "vmull.p8 q1, d2, d11\n\t"                       // 6
        "vext.8 d6, d11, d11, $2\n\t"                    // 7
        "vmull.p8 q3, d10, d6\n\t"                       // 8
        "vext.8 d4, d10, d10, $3\n\t"                    // 9
        "vmull.p8 q2, d4, d11\n\t"                       // 10
        "veorq q0, q0, q4\n\t"                           // 11
        "vext.8 d8, d11, d11, $3\n\t"                    // 12
        "vmull.p8 q4, d10, d8\n\t"                       // 13
        "veorq q1, q1, q3\n\t"                           // 14
        "vext.8 d6, d11, d11, $4\n\t"                    // 15
        "vmull.p8 q3, d10, d6\n\t"                       // 16
        "veor d0, d0, d1\n\t"                            // 17
        "vand d1, d1, d14\n\t"                           // 18
        "veor d2, d2, d3\n\t"                            // 19
        "vand d3, d3, d13\n\t"                           // 20
        "veorq q2, q2, q4\n\t"                           // 21
        "veor d0, d0, d1\n\t"                            // 22
        "veor d2, d2, d3\n\t"                            // 23
        "veor d4, d4, d5\n\t"                            // 24
        "vand d5, d5, d12\n\t"                           // 25
        "veor d6, d6, d7\n\t"                            // 26
        "vmov.i64 d7, $0\n\t"                            // 27
        "vext.8 q0, q0, q0, $15\n\t"                     // 28
        "veor d4, d4, d5\n\t"                            // 29
        "vext.8 q1, q1, q1, $14\n\t"                     // 30
        "vmull.p8 q4, d10, d11\n\t"                      // 31
        "vext.8 q2, q2, q2, $13\n\t"                     // 32
        "vext.8 q3, q3, q3, $12\n\t"                     // 33
        "veorq q0, q0, q1\n\t"                           // 34
        "veorq q2, q2, q3\n\t"                           // 35
        "veorq q4, q4, q0\n\t"                           // 36
        "veorq q4, q4, q2"                               // 37
        :                                                /*output*/
        : "w"(k16), "w"(k32), "w"(k48), "w"(ad), "w"(bd) /*inputs*/
        :                                                /*clobber*/
        );
    return rq;
}
#else

static __v16u mul128 (uint64x1_t a, uint64x1_t b) {
    register uint64x2_t q0 asm("v0") = vdupq_n_u64 (0); // d0, d1
    register uint64x2_t q1 asm("v1") = vdupq_n_u64 (0); // d2, d3
    register uint64x2_t q2 asm("v2") = vdupq_n_u64 (0); // d4, d5
    register uint64x2_t q3 asm("v3") = vdupq_n_u64 (0); // d6, d7
    register poly16x8_t rq asm("v4") = vdupq_n_p16 (0);
    register uint64x2_t k16 asm("v7") = vdupq_n_u64 (0xffff);
    ;                                                                 // d12
    register uint64x2_t k32 asm("v8") = vdupq_n_u64 (0xffffffff);     // d13
    register uint64x2_t k48 asm("v9") = vdupq_n_u64 (0xffffffffffff); // d14

    // 1
    q0 = vsetq_lane_u64 (
    (uint64_t)vreinterpret_u64_u8 (vext_u8 (vreinterpret_u8_u64 (a), vreinterpret_u8_u64 (a), 1)),
    q0, 0);
    // 2
    q0 = vreinterpretq_u64_p16 (
    vmull_p8 (vreinterpret_p8_u64 (b), vreinterpret_p8_u64 (vcreate_u64 (vgetq_lane_u64 (q0, 0)))));
    // 3
    rq = vreinterpretq_p16_u64 (vsetq_lane_u64 (
    (uint64_t)vreinterpret_u64_u8 (vext_u8 (vreinterpret_u8_u64 (b), vreinterpret_u8_u64 (b), 1)),
    vreinterpretq_u64_p16 (rq), 0));
    // 4
    rq =
    vmull_p8 (vreinterpret_p8_u64 (a),
              vreinterpret_p8_u64 (vcreate_u64 (vgetq_lane_u64 (vreinterpretq_u64_p16 (rq), 0))));
    // 5
    q1 = vsetq_lane_u64 (
    (uint64_t)vreinterpret_u64_u8 (vext_u8 (vreinterpret_u8_u64 (a), vreinterpret_u8_u64 (a), 2)),
    q1, 0);
    // 6
    q1 = vreinterpretq_u64_p16 (
    vmull_p8 (vreinterpret_p8_u64 (vcreate_u64 (vgetq_lane_u64 (q1, 0))), vreinterpret_p8_u64 (b)));
    // 7
    q3 = vsetq_lane_u64 (
    (uint64_t)vreinterpret_u64_u8 (vext_u8 (vreinterpret_u8_u64 (b), vreinterpret_u8_u64 (b), 2)),
    q3, 0);
    // 8
    q3 = vreinterpretq_u64_p16 (
    vmull_p8 (vreinterpret_p8_u64 (a), vreinterpret_p8_u64 (vcreate_u64 (vgetq_lane_u64 (q3, 0)))));
    // 9
    q2 = vsetq_lane_u64 (
    (uint64_t)vreinterpret_u64_u8 (vext_u8 (vreinterpret_u8_u64 (a), vreinterpret_u8_u64 (a), 3)),
    q2, 0);
    // 10
    q2 = vreinterpretq_u64_p16 (
    vmull_p8 (vreinterpret_p8_u64 (vcreate_u64 (vgetq_lane_u64 (q2, 0))), vreinterpret_p8_u64 (b)));
    // 11
    q0 = veorq_u64 (q0, vreinterpretq_u64_p16 (rq));
    // 12
    rq = vreinterpretq_p16_u64 (vsetq_lane_u64 (
    (uint64_t)vreinterpret_u64_u8 (vext_u8 (vreinterpret_u8_u64 (b), vreinterpret_u8_u64 (b), 3)),
    vreinterpretq_u64_p16 (rq), 0));
    // 13
    rq =
    vmull_p8 (vreinterpret_p8_u64 (a),
              vreinterpret_p8_u64 (vcreate_u64 (vgetq_lane_u64 (vreinterpretq_u64_p16 (rq), 0))));
    // 14
    q1 = veorq_u64 (q1, q3);
    // 15
    q3 = vsetq_lane_u64 (
    (uint64_t)vreinterpret_u64_u8 (vext_u8 (vreinterpret_u8_u64 (b), vreinterpret_u8_u64 (b), 4)),
    q3, 0);
    // 16
    q3 = vreinterpretq_u64_p16 (
    vmull_p8 (vreinterpret_p8_u64 (vcreate_u64 (vgetq_lane_u64 (q3, 0))), vreinterpret_p8_u64 (a)));
    // 17
    q0 = vsetq_lane_u64 (
    vget_lane_u64 (
    veor_u64 (vcreate_u64 (vgetq_lane_u64 (q0, 0)), vcreate_u64 (vgetq_lane_u64 (q0, 1))), 0),
    q0, 0);
    // 18
    q0 = vsetq_lane_u64 (
    vget_lane_u64 (
    vand_u64 (vcreate_u64 (vgetq_lane_u64 (q0, 1)), vcreate_u64 (vgetq_lane_u64 (k48, 0))), 0),
    q0, 1);
    // 19
    q1 = vsetq_lane_u64 (
    vget_lane_u64 (
    veor_u64 (vcreate_u64 (vgetq_lane_u64 (q1, 0)), vcreate_u64 (vgetq_lane_u64 (q1, 1))), 0),
    q1, 0);
    // 20
    q1 = vsetq_lane_u64 (
    vget_lane_u64 (
    vand_u64 (vcreate_u64 (vgetq_lane_u64 (q1, 1)), vcreate_u64 (vgetq_lane_u64 (k32, 0))), 0),
    q1, 1);
    // 21
    q2 = veorq_u64 (q2, vreinterpretq_u64_p16 (rq));
    // 22
    q0 = vsetq_lane_u64 (
    vget_lane_u64 (
    veor_u64 (vcreate_u64 (vgetq_lane_u64 (q0, 0)), vcreate_u64 (vgetq_lane_u64 (q0, 1))), 0),
    q0, 0);
    // 23
    q1 = vsetq_lane_u64 (
    vget_lane_u64 (
    veor_u64 (vcreate_u64 (vgetq_lane_u64 (q1, 0)), vcreate_u64 (vgetq_lane_u64 (q1, 1))), 0),
    q1, 0);
    // 24
    q2 = vsetq_lane_u64 (
    vget_lane_u64 (
    veor_u64 (vcreate_u64 (vgetq_lane_u64 (q2, 0)), vcreate_u64 (vgetq_lane_u64 (q2, 1))), 0),
    q2, 0);
    // 25
    q2 = vsetq_lane_u64 (
    vget_lane_u64 (
    vand_u64 (vcreate_u64 (vgetq_lane_u64 (q2, 1)), vcreate_u64 (vgetq_lane_u64 (k16, 0))), 0),
    q2, 1);
    // 26
    q3 = vsetq_lane_u64 (
    vget_lane_u64 (
    veor_u64 (vcreate_u64 (vgetq_lane_u64 (q3, 0)), vcreate_u64 (vgetq_lane_u64 (q3, 1))), 0),
    q3, 0);
    // 27
    q3 = vsetq_lane_u64 ((uint64_t)0, q3, 1);
    // 28
    q0 = vreinterpretq_u64_u8 (vextq_u8 (vreinterpretq_u8_u64 (q0), vreinterpretq_u8_u64 (q0), 15));
    // 29
    q2 = vsetq_lane_u64 (
    vget_lane_u64 (
    veor_u64 (vcreate_u64 (vgetq_lane_u64 (q2, 0)), vcreate_u64 (vgetq_lane_u64 (q2, 1))), 0),
    q2, 0);
    // 30
    q1 = vreinterpretq_u64_u8 (vextq_u8 (vreinterpretq_u8_u64 (q1), vreinterpretq_u8_u64 (q1), 14));
    // 31
    rq = vmull_p8 (vreinterpret_p8_u64 (a), vreinterpret_p8_u64 (b));
    // 32
    q2 = vreinterpretq_u64_u8 (vextq_u8 (vreinterpretq_u8_u64 (q2), vreinterpretq_u8_u64 (q2), 13));
    // 33
    q3 = vreinterpretq_u64_u8 (vextq_u8 (vreinterpretq_u8_u64 (q3), vreinterpretq_u8_u64 (q3), 12));
    // 34
    q0 = veorq_u64 (q0, q1);
    // 35
    q2 = veorq_u64 (q2, q3);
    // 36
    rq = vreinterpretq_p16_u64 (veorq_u64 (vreinterpretq_u64_p16 (rq), q0));
    rq = vreinterpretq_p16_u64 (veorq_u64 (vreinterpretq_u64_p16 (rq), q2));

    return vreinterpretq_u16_p16 (rq);
}
#endif

gf_64_t gf64_vmull_state::mul (gf_64_t a64, gf_64_t b64) const {
    uint64x1_t a = vcreate_u64 (a64);
    uint64x1_t b = vcreate_u64 (b64);
    uint64x1_t pp = vcreate_u64 ((uint32_t) (prim_poly & 0xffffffffULL));
    uint64x1_t v;
    uint16x8_t w, result;

    result = mul128 (a, b);
    v = vreinterpret_u64_u32 (vset_lane_u32 (0, vreinterpret_u32_u16 (vget_high_u16 (result)), 0));
    w = mul128 (pp, v);

    result = veorq_u16 (result, w);
    v = vreinterpret_u64_u32 (vset_lane_u32 (0, vreinterpret_u32_u16 (vget_high_u16 (result)), 1));
    w = mul128 (pp, v);

    result = veorq_u16 (result, w);
    return (gf_64_t)vget_low_u16 (result);
}

unsigned gf64_vmull_state::mult_table_size () const { return (0); }

void gf64_vmull_state::mul_region (const gf_64_t *src,
                                   gf_64_t *dst,
                                   uint64_t bytes,
                                   gf_64_t m,
                                   bool accum,
                                   void *mult_table,
                                   bool mult_table_valid) const {
    if (m == (gf_64_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (m == (gf_64_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t chunks = bytes >> 3ULL;
            for (uint64_t i = 0; i < chunks; i += 2) {
                __v64u x = vld1q_u64 (src + i);
                __v64u y = vld1q_u64 (dst + i);
                vst1q_u64 (dst + i, veorq_u64 (x, y));
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    uint64_t i;
    uint64_t chunks = bytes >> 3ULL;

    uint64x1_t x, y;
    if (accum) {
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src + i);
            y = vld1_u64 (dst + i);
            vst1_u64 (dst + i, veor_u64 (vcreate_u64 (mul (vget_lane_u64 (x, 0), m)), y));
        }
    } else {
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src + i);
            vst1_u64 (dst + i, vcreate_u64 (mul (vget_lane_u64 (x, 0), m)));
        }
    }
}

void gf64_vmull_state::mul_region_region (const gf_64_t *src1,
                                          const gf_64_t *src2,
                                          gf_64_t *dst,
                                          uint64_t bytes,
                                          bool accum) const {

    uint64_t i;
    uint64_t chunks = bytes >> 3ULL;

    if (accum) {
        uint64x1_t x, y, z;
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src1 + i);
            y = vld1_u64 (src2 + i);
            z = vld1_u64 (dst + i);
            vst1_u64 (dst + i,
                      veor_u64 (vcreate_u64 (mul (vget_lane_u64 (x, 0), vget_lane_u64 (y, 0))), z));
        }
    } else {
        uint64x1_t x, y;
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src1 + i);
            y = vld1_u64 (src2 + i);
            vst1_u64 (dst + i, vcreate_u64 (mul (vget_lane_u64 (x, 0), vget_lane_u64 (y, 0))));
        }
    }
}

gf_64_t gf64_vmull_state::dotproduct (const gf_64_t *src1,
                                      const gf_64_t *src2,
                                      uint64_t n_elements,
                                      uint64_t stride) const {
    gf_64_t dp = (gf_64_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}
