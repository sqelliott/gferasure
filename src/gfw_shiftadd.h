/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
// gfw_shiftadd.h
// w-bit Galois field using basic shift-add (unoptimized).
//

#pragma once

#include "gf_random.h"
#include "gf_types.h"
#include <stdio.h>

template < class gf_w_t > struct alignas (16) gfw_shiftadd_state {

    // We can't provide predefined defaults for prim_poly or alpha because we
    // don't know the field size.  There's nothing else to initialize because
    // we need to do brute-force on just about everything.
    gfw_shiftadd_state (gf_w_t prim_poly_, gf_w_t alpha_ = (gf_w_t)1)
    : prim_poly (prim_poly_), alpha (alpha_) {}

    // Basic arithmetic is all we need to support
    gf_w_t add (gf_w_t a, gf_w_t b) const { return ((gf_w_t) (a ^ b)); }
    gf_w_t sub (gf_w_t a, gf_w_t b) const { return ((gf_w_t) (a ^ b)); }
    gf_w_t inv (gf_w_t a) const;
    gf_w_t pow (gf_w_t a, uint64_t b) const;
    gf_w_t mul (gf_w_t a, gf_w_t b) const;
    gf_w_t div (gf_w_t a, gf_w_t b) const { return (mul (a, inv (b))); }

    void mul_region (const gf_w_t *src,
                     gf_w_t *dst,
                     uint64_t bytes,
                     gf_w_t m,
                     bool accum = false,
                     void *mult_table = NULL,
                     bool mult_table_valid = false) const;
    void mul_region_region (const gf_w_t *src1,
                            const gf_w_t *src2,
                            gf_w_t *dst,
                            uint64_t bytes,
                            bool accum = false) const;
    gf_w_t dotproduct (const gf_w_t *src1,
                       const gf_w_t *src2,
                       uint64_t n_elements,
                       uint64_t stride = 1) const;

    uint64_t bitwidth () const { return (sizeof (gf_w_t) * 8ULL); }
    // Calculate this by getting bitwidth-1 set bits, and ORing with the leftmost bit set
    uint64_t field_max () const {
        return (((1ULL << (bitwidth () - 1)) - 1) | (1ULL << (bitwidth () - 1)));
    }
    unsigned mult_table_size () const;

    private:
    gf_w_t prim_poly;
    gf_w_t alpha;
    gf_w_t mul_by_2 (gf_w_t a) const {
        bool reduce = !!(a & (gf_w_t) (1ULL << (bitwidth () - 1)));
        return ((a << (gf_w_t)1) ^ (reduce ? prim_poly : (gf_w_t)0));
    }
};

typedef gfw_shiftadd_state< gf_8_t > gf8_shiftadd_state;
typedef gfw_shiftadd_state< gf_16_t > gf16_shiftadd_state;
typedef gfw_shiftadd_state< gf_32_t > gf32_shiftadd_state;
typedef gfw_shiftadd_state< gf_64_t > gf64_shiftadd_state;

extern gf8_shiftadd_state gf8_shiftadd_field;
extern gf16_shiftadd_state gf16_shiftadd_field;
extern gf32_shiftadd_state gf32_shiftadd_field;
extern gf64_shiftadd_state gf64_shiftadd_field;
