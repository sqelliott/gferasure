import subprocess
import numpy as np
import scipy
import pprint

sizes = []
#for x in range(10,25): # faster fields
for x in range(14,17): # slower fields
  sizes.append(2**x)

runs = 10 ## slower fields
#runs = 10 ## average over 1000 runs
oper = "Tm"
field = "gf64_vmull"

results = {}

for size in sizes:
  temp_results = []
  for i in range(0,runs):
    p = subprocess.Popen(["./gf_test", "-%s" % oper, "-b", "%s" % size, "-t","1000", "-g","%s" % field],\
                          stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    for line in str(out).split("\n"):
      if line:
        tres = float(line.split("(")[1][:-1].split(" ")[0])
        ## take bytes * trials / MB
        temp_results.append(tres)

  avg   = float(np.mean(temp_results))
  dev   = float(np.std(temp_results))
  results[size] = {"Average":avg,"Std Dev":dev}
  print(size)
  pprint.pprint(results[size])
pprint.pprint(results)
