#include <iomanip>  //setw
#include <iostream> //cout
#include <string.h> //memcpy

#include "gf_erasure.h"
#include <stdlib.h>

int main ( )
{

  // survive 3 failures
  const int ECC = 3;
  // given a 5 data split
  const int DATA_ROWS = 5;
  gf_8_t **darr;    // immutable data
  gf_8_t **rebuilt; // verify after rebuild

  // RE will be total size
  const int RE = ECC + DATA_ROWS;

  // create an RE vector to hold data + ecc
  gf_8_t **re_array;
  gf_8_t **modarray;

  // initialize our arrays
  darr = new gf_8_t *[DATA_ROWS];
  rebuilt = new gf_8_t *[DATA_ROWS];
  re_array = new gf_8_t *[RE];
  modarray = new gf_8_t *[RE];

  // initialize second dimension
  for (int i = 0; i < DATA_ROWS; i++)
  {
    darr[i] = new gf_8_t[DATA_ROWS];
    rebuilt[i] = new gf_8_t[DATA_ROWS];
  }
  for (int i = 0; i < RE; i++)
  {
    re_array[i] = new gf_8_t[DATA_ROWS];
    modarray[i] = new gf_8_t[DATA_ROWS];
  }

  /* Fill data array with non-random data 1-n, square filled */
  gf_8_t count = 1;
  for (int i = 0; i < DATA_ROWS; i++)
  {
    for (int j = 0; j < DATA_ROWS; j++)
    {
      darr[i][j] = count;
      count += 1;
    }
  }

  /* Print out the matrix - make sure we didnt mess up =) */
  std::cout << "Data:" << std::endl;
  for (int i = 0; i < DATA_ROWS; i++)
  {
    for (int j = 0; j < DATA_ROWS; j++)
    {
      std::cout << std::setw (2) << (int)darr[i][j] << " ";
    }
    std::cout << std::endl;
  }

  /* Make data vector a constant - Dangerous and not recommended casting */
  const gf_8_t **data = const_cast< const gf_8_t ** > (darr);

  /* Pick the distribution matrix type */
  /* Here we choose shiftadd because its not architecture specific */

  gf_erasure< gf_8_t, gf8_shiftadd_state > erasure ((unsigned)DATA_ROWS, (unsigned)RE - DATA_ROWS,
                                                    ECC_CAUCHY);
  // gf_erasure<gf_8_t, gf8_shiftadd_state> erasure ((unsigned)DATA_ROWS, (unsigned)RE-DATA_ROWS,
  // ECC_VANDERMONDE);
  // gf_erasure<gf_8_t, gf8_shiftadd_state> erasure ((unsigned)DATA_ROWS,(unsigned)DATA_ROWS,
  // ECC_CAUCHY_SYSTEMATIC);

  std::cout << "\nEncode the Data in Matrix \n" << std::endl;
  // pass in constant data, output encdoed matrix, and bytes of data
  // because we are using gf_8_t, bytes = matrix size, if gf_16_t, double it, etc.
  erasure.generate (data, re_array, DATA_ROWS * DATA_ROWS);


  // Lets for fun print out our encoded array
  std::cout << "Encoded:" << std::endl;
  for (int i = 0; i < RE; i++)
  {
    for (int j = 0; j < DATA_ROWS; j++)
    {
      std::cout << std::setw (4) << (int)re_array[i][j] << " ";
    }
    std::cout << std::endl;
  }

  std::cout << std::endl;

  /*
  //this is not necessary, but to illistrate that we have a loss in our encoded matrix
  std::cout << "Deleting elements" << std::endl;
  for (int i =0;i < RE; i++){
    for (int j =0;j < DATA_ROWS; j++){
      std::cout << std::setw(3) << (int)re_array[i][j] << " ";
    }
    std::cout << "   ->   " ;
    for (int j =0;j < DATA_ROWS; j++){
      if (i%3==0){
        re_array[i][j] = 0;
      }
      std::cout << std::setw(3) << (int)re_array[i][j] << " ";
    }
    std::cout << std::endl;
  }

  std::cout << std::endl;
  std::cout << "Working Rows: ";
  gf_8_t ids[DATA_ROWS];
  int x = 0;
  for (int j =0;j < RE; j++){
    if (!(j%ECC==0)){
      std::cout << j << " ";
      ids[j]= j;
      x++;
    }
  }
  std::cout <<std::endl;
  */


  // would like to make it so rebuild can take in non-square matrix
  gf_8_t ids[RE];
  for (int i = 0; i < RE; ++i)
  {
    ids[i] = i;
  }

  // swap the rows
  for (int i = 0; i < ECC; i++)
  {
    gf_8_t r = gf_random_u64 ( ) % (RE - 1);
    gf_8_t t = ids[i];
    ids[i] = ids[r];
    ids[r] = t;
  }

  // make same swaps for matrix, so now we need to rebuild data
  for (int i = 0; i < RE; ++i)
  {
    modarray[i] = re_array[ids[i]];
  }


  // generate the rebuild matrix based on the surviving rows
  erasure.config_rebuild (ids);

  // rebuild the original data from our surviving data, requires solving linear equations + invert
  erasure.rebuild (const_cast< const gf_8_t ** > (modarray), rebuilt, DATA_ROWS * DATA_ROWS);

  std::cout << std::endl;

  std::cout << "After Rebuild:" << std::endl;
  for (int i = 0; i < DATA_ROWS; i++)
  {
    for (int j = 0; j < DATA_ROWS; j++)
    {
      std::cout << std::setw (4) << (int)rebuilt[i][j] << " ";
    }
    std::cout << std::endl;
  }

  // Check in memory to compare that the original data (data) matches the rebuilt (rebuilt)
  for (int j = 0; j < DATA_ROWS; ++j)
  {
    if (memcmp (rebuilt[j], data[j], DATA_ROWS))
    {
      std::cout << "Rebuild Failed!" << std::endl;
      exit (1);
    }
  }

  // Yay it worked!
  std::cout << "Rebuild Worked!" << std::endl;
}
